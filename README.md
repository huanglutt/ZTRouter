# ZTRouter

#### 介绍
iOS 路由组件

#### 软件架构
软件架构说明

1. 该router框架借鉴了前端vue 的相关写法，是基于iOS原生的UINavigationController实现的
2. 为了方便路由管理，都是通过UINavigationController 实现的，如果要实现 present 效果，后期会通过 过渡动画实现

# 使用方法
```
    pod 'ZTRouter'
```

#### 安装教程
```
    pod 'ZTRouter'
```

#### 使用说明

1. 在Podfile文件中
```
    pod 'ZTRouter'
```
然后再 进入工程目录， 
> 输入 pod install
2. 在pch 文件中导入 ZTRouter.h 文件，或者在需要的地方导入该文件
3. 配载路由
```
    - (void) configRoutes {
        ZTRouterObject *route1 = [[ZTRouterObject alloc] initName:@"red" path:@"/red" initial:true page:[RedViewController class]];
        ZTRouterObject *route2 = [[ZTRouterObject alloc] initName:@"blue" path:@"/blue" page:[BlueViewController class]];
        ZTRouterObject *route3 = [[ZTRouterObject alloc] initName:@"yellow" path:@"/yellow" page:[YellowViewController class]];
        ZTRouterObject *route4 = [[ZTRouterObject alloc] initName:@"black" path:@"/black" page:[BlackViewController class]];
        [[ZTRouterManager shared] registerAllRouter:@[route1 , route2 , route3 , route4]];
    }
```
4. 由于iOS中，路由一般与 ViewController 中做，所以，使用该框架时，可以直接在VC中调用以下api
```
    - (void) push:(NSString *)name param:(id)param;

    - (void) pushToPath:(NSString *)path param:(id)param;

    - (void) replace:(NSString *)name param:(id)param;

    - (void) replaceToPath:(NSString *)path param:(id)param;

    - (void) pop;

    - (void) popToRoot;

    - (void) popToName:(NSString *)name param:(id)param;

    - (void) popToPath:(NSString *)path param:(id)param;
```


--------
    待续，后期会加入各种导航动画，且动画配置
---------

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)