//
//  ZTRouterObject.h
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger , ZTRouterPushMode) {
    ZTRouterPushMode_push = 0, // 普通push效果 从右到左
    ZTRouterPushMode_present , // present效果 从下到上
    ZTRouterPushMode_middleIn, // 中间淡入  从中间进入，逐渐放大
    ZTRouterPushMode_topIn,    // 从上面进入 ， 从上到下
    ZTRouterPushMode_leftIn,   // 从左边进入
    ZTRouterPushMode_drawer_right, // 右抽屉
    ZTRouterPushMode_drawer_left   // 左抽屉
};

@interface ZTRouterObject : NSObject

// 需要导航的路由的名称
@property (nonatomic , copy , readonly) NSString *name;
// 需要导航的路由的路径
@property (nonatomic , copy , readonly) NSString *path;
// 需要导航的路由的页面
@property (nonatomic , strong , readonly) Class page;

@property (nonatomic , assign) id param;

@property (nonatomic , copy) void(^callback)(id response);

@property (nonatomic , assign) BOOL isInitial; // 是否是初始的，即首页显示的

- (instancetype) initName:(NSString *)name
                     path:(NSString *)path
                     page:(Class)page;

- (instancetype) initName:(NSString *)name
                     path:(NSString *)pathis
                  initial:(BOOL)initial
                     page:(Class)page;

@end

NS_ASSUME_NONNULL_END
