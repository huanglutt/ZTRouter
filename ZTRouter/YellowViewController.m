//
//  YellowViewController.m
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import "YellowViewController.h"
#import "ZTRouter.h"

@interface YellowViewController ()

@end

@implementation YellowViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    self.view.backgroundColor = [UIColor yellowColor];UIButton *btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn setTitle:@"tap" forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(tapMe) forControlEvents:UIControlEventTouchUpInside];
    btn.frame = CGRectMake(100, 100, 50, 30);
    [self.view addSubview:btn];
    
    UIButton *btn2 = [UIButton buttonWithType:UIButtonTypeCustom];
    [btn2 setTitle:@"pop root" forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(pophandle) forControlEvents:UIControlEventTouchUpInside];
    btn2.frame = CGRectMake(100, 300, 100, 30);
    [self.view addSubview:btn2];
}

- (void) tapMe {
    [self push:@"black" param:nil];
}

- (void) pophandle {
    [self popToRootForParam:nil completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
