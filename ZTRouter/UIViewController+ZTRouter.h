//
//  UIViewController+ZTRouter.h
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZTRouterObject.h"

NS_ASSUME_NONNULL_BEGIN


typedef NS_ENUM(NSInteger , ZTRouterAnimationType) {
    ZTRouterAnimationType_push = 0,
    ZTRouterAnimationType_present,
    ZTRouterAnimationType_pop,
    ZTRouterAnimationType_middle
};

@interface UIViewController (ZTRouter)

@property (nonatomic , assign) ZTRouterAnimationType animationType;

@property (nonatomic , strong) ZTRouterObject *route;

- (void) push:(NSString *)name param:(nullable id)param;

- (void) pushToPath:(NSString *)path param:(nullable id)param;

- (void) replace:(NSString *)name param:(nullable id)param;

- (void) replaceToPath:(NSString *)path param:(nullable id)param;

- (void) popForParam:(nullable id)param completion:(void(^ __nullable)(void))completion;

- (void) popToRootForParam:(nullable id)param completion:(void(^ __nullable)(void))completion;

- (void) popToName:(NSString *)name param:(nullable id)param  completion:(void(^ __nullable)(void))completion;

- (void) popToPath:(NSString *)path param:(nullable id)param completion:(void(^ __nullable)(void))completion;

@end

NS_ASSUME_NONNULL_END
