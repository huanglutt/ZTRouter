//
//  ZTRouterObject.m
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import "ZTRouterObject.h"

@implementation ZTRouterObject

- (instancetype) initName:(NSString *)name path:(NSString *)path page:(Class)page  {
    if (self = [super init]) {
        _name = name;
        _path = path;
        _page = page;
    }
    return self;
}

- (instancetype) initName:(NSString *)name path:(NSString *)path initial:(BOOL)initial page:(Class)page {
    if (self = [super init]) {
        _name = name;
        _path = path;
        _page = page;
        _isInitial = initial;
    }
    return self;
}

@end
