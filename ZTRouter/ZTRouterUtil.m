//
//  ZTRouterUtil.m
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import "ZTRouterUtil.h"

@implementation ZTRouterUtil

+ (ZTRouterObject *) findVC:(NSString *)name routes:(NSArray<ZTRouterObject *> *)routes {
    __block ZTRouterObject *route = nil;
    [routes enumerateObjectsUsingBlock:^(ZTRouterObject * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.name isEqualToString:name]) {
            route = obj;
            *stop = true;
        }
    }];
    return route;
}

+ (ZTRouterObject *) findVCForPath:(NSString *)path routes:(NSArray<ZTRouterObject *> *)routes {
    __block ZTRouterObject *route = nil;
    [routes enumerateObjectsUsingBlock:^(ZTRouterObject * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([obj.path isEqualToString:path]) {
            route = obj;
            *stop = true;
        }
    }];
    return route;
}

@end
