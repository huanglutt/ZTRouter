//
//  ZTRouterManager.m
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import "ZTRouterManager.h"
#import "ZTRouterUtil.h"
#import "ZTRouteStack.h"
#import "UIViewController+ZTRouter.h"

static NSString * ZTRouterRootStackName = @"ZTRouterRootStackName_333###$$$%%%%()";

static ZTRouterManager *manager;

@interface ZTRouterManager ()

@property (nonatomic , strong) NSMutableArray *currentRouters;

@property (nonatomic , strong) ZTRouteStack *routeStack;

@end

@implementation ZTRouterManager

- (instancetype) init {
    if (self = [super init]) {
        self.currentRouters = [NSMutableArray arrayWithCapacity:MAXFLOAT];
        self.routeStack = [ZTRouteStack shared];
    }
    return self;
}

+ (instancetype) shared {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (!manager) {
            manager = [[ZTRouterManager alloc] init];
        }
    });
    return manager;
}

- (UIViewController *) topStackVC {
    return [self.routeStack topStack];
}

- (void) registerAllRouter:(NSArray<ZTRouterObject *> *)routers {
    [self.currentRouters addObjectsFromArray:routers];
    [self initRootVC];
}

// 如果有初始化的route,那么在注册的时候，将window的rootVC设置为initial
- (void) initRootVC {
    __block UIViewController *initialVC = nil;
    [self.currentRouters enumerateObjectsUsingBlock:^(ZTRouterObject *  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        if (obj.isInitial == true) {
            initialVC = [[obj.page alloc] init];
            *stop = true;
        }
    }];
    if (initialVC) {
        ZTRouteStackObject *stack = [[ZTRouteStackObject alloc] init];
        stack.vc = initialVC;
        [self.routeStack push:stack];
        [UIApplication sharedApplication].keyWindow.rootViewController = initialVC;
    }
}

- (void) addRouter:(ZTRouterObject *)route {
    [self.currentRouters addObject:route];
}

- (void) removeRouteForName:(NSString *)name {
    ZTRouterObject *object = [ZTRouterUtil findVC:name routes:self.currentRouters];
    [self.currentRouters removeObject:object];
}


- (UIViewController *) pushToName:(NSString *)name params:(id)params {
    ZTRouterObject *route = [self routeIsExsit:name];
    if (route) {
        ZTRouteStackObject *stack = [[ZTRouteStackObject alloc] init];
        UIViewController *vc = [[route.page alloc] init];
        stack.vc = vc;
        vc.route = route;
        route.param = params;
        [self.routeStack push:stack];
        return vc;
    }
    NSAssert(false, @"未配置对应的路由数据");
    return nil;
}

- (UIViewController *) pushToPath:(NSString *)path query:(id)query {
    ZTRouterObject *route = [self routePathIsExsit:path];
    if (route) {
        ZTRouteStackObject *stack = [[ZTRouteStackObject alloc] init];
        UIViewController *vc = [[route.page alloc] init];
        stack.vc = vc;
        vc.route = route;
        route.param = query;
        [self.routeStack push:stack];
        return vc;
    }
    NSAssert(false, @"未配置对应的路由数据");
    return nil;
}

- (UIViewController *) popForParam:(id)param {
    return [self.routeStack popForParam:param];
}

- (UIViewController *) popToRootForParam:(id)param {
    return [self.routeStack popToRootForParam:param];
}

- (UIViewController *) replaceToName:(NSString *)name params:(id) params {
    ZTRouterObject *route = [self routePathIsExsit:name];
    if (route) {
        ZTRouteStackObject *stack = [[ZTRouteStackObject alloc] init];
        UIViewController *vc = [[route.page alloc] init];
        stack.vc = vc;
        vc.route = route;
        route.param = params;
        [self.routeStack replaceAndPush:stack];
        return vc;
    }
    NSAssert(false, @"未配置对应的路由数据");
    return nil;
}

- (UIViewController *) replaceToPath:(NSString *)path params:(id) params {
    ZTRouterObject *route = [self routePathIsExsit:path];
    if (route) {
        ZTRouteStackObject *stack = [[ZTRouteStackObject alloc] init];
        UIViewController *vc = [[route.page alloc] init];
        stack.vc = vc;
        vc.route = route;
        route.param = params;
        [self.routeStack replaceAndPush:stack];
        return vc;
    }
    NSAssert(false, @"未配置对应的路由数据");
    return nil;
}

- (UIViewController *) popToName:(NSString *)name params:(id) params {
    UIViewController *vc = [self.routeStack popToName:name param:params];
    if (vc) {
        return vc;
    }
    NSLog(@"未配置对应的路由数据");
    return nil;
}

- (UIViewController *) popToPath:(NSString *)path params:(id) params {
    UIViewController *vc = [self.routeStack popToPath:path param:params];
    if (vc) {
        return vc;
    }
    NSLog(@"未配置对应的路由数据");
    return nil;
}

- (ZTRouterObject *) routeIsExsit:(NSString *)name {
    ZTRouterObject *route = [ZTRouterUtil findVC:name routes:self.currentRouters];
    return route;
}

- (ZTRouterObject *) routePathIsExsit:(NSString *)path {
    ZTRouterObject *route = [ZTRouterUtil findVCForPath:path routes:self.currentRouters];
    return route;
}

@end
