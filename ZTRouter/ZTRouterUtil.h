//
//  ZTRouterUtil.h
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZTRouterObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZTRouterUtil : NSObject

+ (ZTRouterObject *) findVC:(NSString *)name routes:(NSArray<ZTRouterObject *> *)routes;

+ (ZTRouterObject *) findVCForPath:(NSString *)path routes:(NSArray<ZTRouterObject *> *)routes;

@end

NS_ASSUME_NONNULL_END
