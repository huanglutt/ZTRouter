//
//  ZTRouteStack.h
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ZTRouterObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZTRouteStackObject : NSObject

@property (nonatomic , strong) UIViewController *vc;

@end

@interface ZTRouteStack : NSObject

+ (instancetype) shared;

- (UIViewController *) popForParam:(nullable id)param;

- (UIViewController *) popToRootForParam:(nullable id)param;

- (UIViewController *) popToName:(NSString *)name param:(nullable id)param;

- (UIViewController *) popToPath:(NSString *)path param:(nullable id)param;

// 获取路由栈底部的vc
- (UIViewController *) topStack;

- (void) push:(ZTRouteStackObject *)route;

- (void) replaceAndPush:(ZTRouteStackObject *)route;

@end

NS_ASSUME_NONNULL_END
