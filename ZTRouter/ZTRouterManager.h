//
//  ZTRouterManager.h
//  ZTRouter
//
//  Created by 黄露 on 2019/5/30.
//  Copyright © 2019 huanglu. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ZTRouterObject.h"

NS_ASSUME_NONNULL_BEGIN

@interface ZTRouterManager : NSObject

+ (instancetype) shared;

- (UIViewController *) topStackVC;

- (void) registerAllRouter:(NSArray<ZTRouterObject *> *)routers;

- (void) addRouter:(ZTRouterObject *)route;

- (void) removeRouteForName:(NSString *)name;

- (UIViewController *) pushToName:(NSString *)name params:(id)params;

- (UIViewController *) pushToPath:(NSString *)path query:(id)query;

- (UIViewController *) replaceToName:(NSString *)name params:(id) params;

- (UIViewController *) replaceToPath:(NSString *)path params:(id) params;

- (UIViewController *) popForParam:(id)param;

- (UIViewController *) popToRootForParam:(id)param;

- (UIViewController *) popToName:(NSString *)name params:(id) params;

- (UIViewController *) popToPath:(NSString *)path params:(id) params;

@end

NS_ASSUME_NONNULL_END
