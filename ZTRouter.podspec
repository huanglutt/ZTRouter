Pod::Spec.new do |s|

  s.name         = "ZTRouter"
  s.version      = "0.1.5"
  s.summary      = "iOS的路由封装，逐步完善中"

  s.homepage     = "https://gitee.com/huanglutt/ZTRouter"
  s.license      = 'MIT'
  s.author       = { "Lucky Huang" => "583699255@qq.com" }
  s.platform     = :ios, "8.0"
  s.ios.deployment_target = "8.0"
  s.source       = { :git => "git@gitee.com:huanglutt/ZTRouter.git", :tag => s.version}
 
  s.requires_arc = true
  
  s.source_files = 'ZTRouter/ZTRouter.h'


   # router
   s.subspec 'router' do |r|

    r.source_files = 'ZTRouter/ZTRoute*.*', 'ZTRouter/UIViewController+ZTRouter.{h,m}','ZTRouter/Transition/**/*.{h,m}'

   end

  # 提交命令
   # 语法验证
   # pod spec lint ZTRouter.podspec --use-libraries --allow-warnings --verbose
   # 提交 
   # pod trunk push ZTRouter.podspec --use-libraries --allow-warnings --verbose

   # git tag -d test_tag　　　　　　　　//本地删除tag
   # git push origin :refs/tags/test_tag　　　　//本地tag删除了，再执行该句，删除线上tag

end
